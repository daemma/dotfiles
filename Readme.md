<!-- ####################################################################### -->
<!-- @file       Readme.md -->
<!-- @brief      Readme for dotfiles -->
<!-- @author     0xD62EE11516877AA8 -->
<!-- @date       2015-03-05 -->
<!-- @copyright  GPLv3+ -->

[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/daemma/dotfiles/blob/master/License.txt)
![Version](https://img.shields.io/:version-2.0.1-green.svg)

# Some .dotfiles
Intended as a repository for my own use, but others may find this useful.

## Dependencies
[GNU stow](https://www.gnu.org/software/stow/stow.html) is a software which 
allows you to create symbolic links out of one base directory.

## Install
```bash
$ ./install.sh
```
Note that `stow` will steadfastly refuse to overwright files which it 
does not "own". You will, therefore, want to remove (and back up!) your 
current relevant dotfiles. You can, of course, edit the files in 
the repository to match your own specifications.

## Modify Specifics
You may want to modify some `daemma` specifics, i.e. `shell/.shell.d/pii.sh`.

## License
![GPLv3](https://gnu.org/graphics/gplv3-88x31.png)

`Copyright (C) 2016 0xD62EE11516877AA8`

`dotfiles` is software libre; 
you are free to modify and share this software under the terms of the 
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html), 
or greater; 
see [License.txt](https://gitlab.com/daemma/dotfiles/blob/master/License.txt)
for details.

### Inspiration
This software is based on and was developed using the following packages:

 * [GNU stow template](https://github.com/Paradiesstaub/gnu_stow_template):
   The MIT License (MIT), Copyright (c) 2014 Thibaut Brandscheid
 * [Mathias’s dotfiles](https://github.com/mathiasbynens/dotfiles):
   The MIT License (MIT), Copyright Mathias Bynens <http://mathiasbynens.be/>

### Contains
This project contains the following packages/plugins/files:

 * [align-string](http://www.pvv.org/~markusk/align-string.el):
   GPLv3+, Copyright (c) 2001 Markus Bjartveit Krüger
 * [auto-pair](http://autopair.googlecode.com):
   GPLv3+, Copyright (C) 2009,2010 Joao Tavora
 * [cmake-mode](http://www.cmake.org/CMakeDocs/cmake-mode.el):
   BSD License, Copyright 2000-2009 Kitware, Inc., Insight Software Consortium
 * [color-theme](http://www.emacswiki.org/cgi-bin/wiki.pl?ColorTheme):
   GPLv2+, Copyright (C) 1999 Jonadab the Unsightly One, et.al.
 * [color-theme-tango](http://www.emacswiki.org/emacs/color-theme-tango.el):
   Created by danranx@gmail.com.
 * [markdown-mode](http://jblevins.org/projects/markdown-mode/):
   GPLv2+, Copyright (C) 2007-2011 Jason R. Blevins
 * [yasnippet-bundle](http://code.google.com/p/yasnippet/):
   GPLv2+, Copyright 2009 pluskid, joaotavora

 * The project avatar is [terminal-512.png](https://cdn4.iconfinder.com/data/icons/eldorado-multimedia/40/terminal-512.png)
   by [Icojam](https://www.iconfinder.com/Icojam) 
   and used as *fair-use* under the [iconfinder.com basic license](https://www.iconfinder.com/licenses/basic).

<!--  end Readme.md -->
<!-- ####################################################################### -->
