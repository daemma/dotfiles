### ############################################################################
##! @file       .bash_profile
##! @brief      Sourced by bash at login
##! @author     0xD62EE11516877AA8
##! @date       2016-09-17
##! @copyright  GPLv3+

## Source .bashrc @ _login_
[ -r ~/.bashrc ] && source ~/.bashrc

### end .bash_profile
### ############################################################################
