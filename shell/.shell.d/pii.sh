#!  /usr/bin/env bash
### ############################################################################
##! @file       pii.sh
##! @brief      Personally identifyable information
##! @author     0xD62EE11516877AA8
##! @date       2016-09-16
##! @copyright  GPLv3+

export FULLNAME="Emma H"
export HANDLE="daemma"
export EMAIL="${HANDLE}@riseup.net"
export KEY="0xD62EE11516877AA8"
export FPR="99DB AE95 5566 2741 A00D  F7AF D62E E115 1687 7AA8"
export CONTACT="\"${FULLNAME}\" <${EMAIL}> (${KEY})"

### end pii.sh
### ############################################################################
