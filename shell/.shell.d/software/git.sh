#!  /usr/bin/env bash
### ############################################################################
##! @file       git.sh
##! @brief      git configuration and helpers
##! @author     0xD62EE11516877AA8
##! @date       2016-09-16
##! @copyright  GPLv3+

export GIT_AUTHOR_NAME="${HANDLE}"
export GIT_AUTHOR_EMAIL="${EMAIL}"
export GIT_COMMITTER_NAME="${GIT_AUTHOR_NAME}"
export GIT_COMMITTER_EMAIL="${GIT_AUTHOR_EMAIL}"

### end git.sh
### ############################################################################
